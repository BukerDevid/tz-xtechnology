package models

//ModelQuoteRes - модель биржевых показателей котировок (Responce)
type ModelQuoteRes struct {
	Price     float64 `json:"price"`
	Volume    float64 `json:"volume"`
	LastTrade float64 `json:"last_trade"`
}

//ModelQuoteReq - модель биржевых показателей котировок (Responce)
type ModelQuoteReq struct {
	Symbol    string  `json:"symbol"`
	Price     float64 `json:"price_24h"`
	Volume    float64 `json:"volume_24h"`
	LastTrade float64 `json:"last_trade_price"`
}
