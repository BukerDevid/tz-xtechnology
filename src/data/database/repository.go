package database

import (
	"encoding/json"
	"src/data/models"
)

//WritetTickers - запись котировок в БД
func WriteTickers(root *Database, quotes []models.ModelQuoteReq) error {
	return root.DatabaseWriteQuotes(quotes)
}

//ReadTickers - Чтоние котировок из БД
func ReadTickers(root *Database) ([]byte, error) {
	quotes, err := root.DatabaseReadQuotes()
	if err != nil {
		return nil, err
	}
	return ParseQuotesForResponce(quotes)
}

//ParseQuotesForResponce - парсинг данных из БД для клиента (json object return)
func ParseQuotesForResponce(quotes []models.ModelQuoteReq) ([]byte, error) {
	var objectQuotes = make(map[string]models.ModelQuoteRes, 200)
	for _, quote := range quotes {
		objectQuotes[quote.Symbol] = models.ModelQuoteRes{
			Volume:    quote.Volume,
			Price:     quote.Price,
			LastTrade: quote.LastTrade,
		}
	}
	return json.Marshal(objectQuotes)
}
