package database

import (
	"context"

	"src/utility"

	pgxp "github.com/jackc/pgx/v4/pgxpool"
)

var (
	errorPQX = "Database driver PGX# "
)

type Database struct {
	connectionStrPG string
	dbConnectPool   *pgxp.Pool
}

//DBInstance - инициализирует ссылку подключения к бд
func DBInstance(connStrPG string) *Database {
	return &Database{connectionStrPG: connStrPG}
}

//InitDatabaseConnections - инициализарует подключения к бд
func (db *Database) InitDatabaseConnections() error {
	confConn, err := pgxp.ParseConfig(db.connectionStrPG) // Example: user=jack password=secret host=host1,host2,host3 port=5432,5433,5434 dbname=mydb sslmode=verify-ca
	if err != nil {                                       // https://www.postgresql.org/docs/11/libpq-connect.html#LIBPQ-MULTIPLE-HOSTS
		return utility.ErrorCreate(errorPQX+" ParseURI ", err.Error())
	}
	db.dbConnectPool, err = pgxp.ConnectConfig(context.Background(), confConn)
	if err != nil {
		return utility.ErrorCreate(errorPQX+" NewConnPool ", err.Error())
	}
	return nil
}
