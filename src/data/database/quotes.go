package database

import (
	"context"
	"src/data/models"
)

//DatabaseWriteQuotes - выгрузка списка курсов по фильтру
func (db *Database) DatabaseWriteQuotes(quotes []models.ModelQuoteReq) error {
	connectDatabase, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return err
	}
	defer connectDatabase.Release()
	//Инициализация транзакции для оптимизированного заполнения таблицы данными (Стоит оптимизировать занесения данных в таблицу)
	txDB, err := connectDatabase.Begin(context.Background())
	if err != nil {
		return err
	}
	for _, data := range quotes {
		if _, err := txDB.Exec(context.Background(), "INSERT INTO quotes24(quote,price,volume,last_trade) VALUES($1,$2,$3,$4)",
			data.Symbol,
			data.Price,
			data.Volume,
			data.LastTrade); err != nil {
			txDB.Rollback(context.Background())
			return err
		}
	}
	txDB.Commit(context.Background())
	return nil
}

//DatabaseReadQuotes - выгрузка списка курсов по фильтру
func (db *Database) DatabaseReadQuotes() ([]models.ModelQuoteReq, error) {
	connectDatabase, err := db.dbConnectPool.Acquire(context.Background())
	if err != nil {
		return nil, err
	}
	defer connectDatabase.Release()
	res, err := connectDatabase.Query(context.Background(), "SELECT quote, price, volume, last_trade FROM quotes24")
	if err != nil {
		return nil, err
	}
	var quotes = make([]models.ModelQuoteReq, 0)
	for res.Next() {
		var quote models.ModelQuoteReq
		err = res.Scan(&quote.Symbol, &quote.Price, &quote.Volume, &quote.LastTrade)
		if err != nil {
			return quotes, err
		}
		quotes = append(quotes, quote)
	}
	return quotes, nil
}
