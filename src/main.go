package main

import (
	"fmt"
	"log"
	"os"

	"src/core/server"
	"src/utility"
)

var pathConfiguration = "config.json"
var pathLogFile = "logerr.log"
var quit = make(chan os.Signal)
var errChan = make(chan string)
var actionChan = make(chan interface{})

func main() {
	err := utility.CreateLog(pathLogFile)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if errCore := server.InitCore(pathConfiguration); errCore != nil {
		log.Fatal(errCore) // Ошибка инициализации сервера
	}
	fmt.Println("X-TECHNOLOGY TZ Server - autor mr.wgw@yandex.ru")
	//Горутина для сервисов
	go func() {
		if errSt := server.Start(errChan, actionChan); errSt != nil {
			fmt.Println(errSt)
			os.Exit(1)
		}
	}()
	//Горутина для логирования
	go func() {
		for {
			select {
			case val := <-errChan:
				utility.WriteLog(val)
				// case <-quit:
				// 	return
			}
		}
	}()
	<-quit
}
