package server

import (
	"src/core/client"
	"src/data/database"
	"src/utility"
)

var errorCore = "Core module error#"

var core Core

//Core - Объект ядра сервера для удобного взаимодействия со всеми основными модулями
type Core struct {
	conf *utility.Configuration //Object configuration
	proc *HTTPProcess           //Object server
	db   *database.Database     //Object database connect
}

//InitCore - Инициализация ядра
func InitCore(path string) error {
	conf, err := utility.LoadFile(path) //Read config file
	if err != nil {
		return err
	}
	if !conf.IsValid() {
		return utility.ErrorCreate(errorCore, "InitCore - конфигурационный файл поврежден или пуст!")
	}
	core = Core{conf, InitServer(), database.DBInstance(conf.ConnectStrPg)}
	return nil
}

//Start - Запуск всех процессов
func Start(log chan<- string, act chan interface{}) error {
	//Подключение к БД
	if errDb := core.db.InitDatabaseConnections(); errDb != nil {
		log <- errDb.Error()
		return errDb
	}
	//Динамичное обновление биржевых котировок
	go client.UpdateTikers(30, core.db, log)
	core.Handlers(log)
	if errProc := core.proc.StartProcess(core.conf); errProc != nil {
		log <- errProc.Error()
		return errProc
	}
	return nil
}
