package server

import (
	"net/http"
	"src/data/database"
)

//Handlers - методы сервера
func (root *Core) Handlers(errCh chan<- string) {
	//Проверка сервера
	root.proc.router.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("x-tech server"))
	}).Methods("GET")
	//Выгрузка катеровок
	root.proc.router.HandleFunc("/api/tickers", func(w http.ResponseWriter, r *http.Request) { //READY
		data, err := database.ReadTickers(root.db)
		if err == nil {
			w.Header().Set("Content-Type", "application/json")
			w.Write(data)
		} else {
			w.Header().Add("Err", "Server error")
			w.WriteHeader(400)
		}
	}).Methods("GET")
}
