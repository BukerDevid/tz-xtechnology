package server

import (
	"bufio"
	"log"
	"net"
	"net/http"
	"os"

	"src/utility"

	"github.com/gorilla/mux"
	"golang.org/x/net/http2"
)

var errorHTTP = "Http process error# "

//HTTPProcess - struct server
type HTTPProcess struct {
	router *mux.Router
	server *http.Server
}

//InitServer - init http server
func InitServer() *HTTPProcess {
	rout := mux.NewRouter()
	file, _ := os.Create("web.log")
	writeInFile := bufio.NewWriter(file)
	server := &http.Server{
		ErrorLog: log.New(writeInFile, "log: ", 0),
		Handler:  rout,
	}
	return &HTTPProcess{rout, server}
}

//StartProcess - запуск сервера
func (root *HTTPProcess) StartProcess(conf *utility.Configuration) error {
	http2Server := http2.Server{}
	err := http2.ConfigureServer(root.server, &http2Server)
	if err != nil {
		return utility.ErrorCreate(errorHTTP+" StartProcess.ConfigureServer ", err.Error())
	}
	listen, err := net.Listen("tcp", conf.AddressServ)
	if err != nil {
		return utility.ErrorCreate(errorHTTP+" StartProcess.Net.Listen ", err.Error())
	}
	if err := root.server.Serve(listen); err != nil {
		return utility.ErrorCreate(errorHTTP+" StartProcess.ServeTLS ", err.Error())
	}
	return nil
}
