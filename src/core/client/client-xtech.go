package client

import (
	"encoding/json"
	"fmt"
	"net/http"
	"src/data/database"
	"src/data/models"
	"time"
)

//UpdateTikers - worker для регулярного обновления данных в базе данных
func UpdateTikers(timer int, db *database.Database, log chan<- string) {
	var tickersClient = &http.Client{Timeout: 10 * time.Second}
	fmt.Println("Start worker - update quotes...")
	var quote = make([]models.ModelQuoteReq, 0)
	for {
		resp, err := tickersClient.Get("https://api.blockchain.com/v3/exchange/tickers")
		if err != nil {
			log <- err.Error()
			time.Sleep(time.Second * 2)
			continue
		}
		err = json.NewDecoder(resp.Body).Decode(&quote)
		if err != nil {
			log <- err.Error()
			time.Sleep(time.Second * 2)
			resp.Body.Close()
			continue
		}
		database.WriteTickers(db, quote)
		time.Sleep(time.Second * time.Duration(5))
		resp.Body.Close()
	}
}
