package utility

import (
	"encoding/json"
	"io/ioutil"
)

//Configuration - структура объекта конфигурации
type Configuration struct {
	ConnectStrPg string `json:"connect-postgres-database"`
	AddressServ  string `json:"address-server"`
}

//IsValid - проверка основных полей конфигураций
func (obj *Configuration) IsValid() bool {
	if obj != nil {
		return (obj.AddressServ != "" && obj.ConnectStrPg != "")
	}
	return false
}

//LoadFile - загрузка файла конфигурации
func LoadFile(path string) (*Configuration, error) {
	fileBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, ErrorCreate("Error cload congifure file", err.Error())
	}
	return unmarshalToObjConf(fileBytes)
}

//UnmarshalToObjConf - чтение конфигурации
func unmarshalToObjConf(confBytes []byte) (*Configuration, error) {
	var config Configuration
	if err := json.Unmarshal(confBytes, &config); err != nil {
		return nil, ErrorCreate("Error cload congifure file", err.Error())
	}
	return &config, nil
}
