package utility

import (
	"errors"
	"strings"
)

//ErrorCreate make custom mistake
func ErrorCreate(moduleName string, err string) error {
	strErr := strings.Join([]string{moduleName, err}, " * ")
	return errors.New(strErr)
}
